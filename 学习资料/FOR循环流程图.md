    FOR 循环的流程图如下
    for( init ; cond ; step)
    {
        op;
    }

---

    ```flow
    st=>start: 开始
    pre=>operation: 初始化循环(init)
    check=>condition: 检查循环是否结束?(cond)
    e=>end: 结束循环
    oper=>operation: 循环体(op)
    step=>operation: 循环步骤(step)
    
    st->pre->check
    check(no,bottom)->oper(bottom)->step(left)->check
    check(yes)->e
    ```


```flow
st=>start: 开始
pre=>operation: 初始化循环(init)
check=>condition: 检查循环是否结束?(cond)
e=>end: 结束循环
oper=>operation: 循环体(op)
step=>operation: 循环步骤(step)
    
st->pre->check
check(no,bottom)->oper(bottom)->step(left)->check
check(yes)->e
```
