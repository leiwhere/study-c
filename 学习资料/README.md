# 学习资料 

* [markdown 语法规则](https://www.jianshu.com/p/191d1e21f7ed)
* [cmd-markdown-高阶语法规则(flow 流程图)](https://www.zybuluo.com/mdeditor?url=https://www.zybuluo.com/static/editor/md-help.markdown#cmd-markdown-高阶语法手册)
* [makedown 在线编辑(mermaid 流程图,abc 五线谱)](https://github.com/nicejade/markdown-online-editor)
---
* [多语言在线开发环境](http://codepad.org/)
* [在线 python 开发环境](http://www.pythontip.com/coding/run)
* [在线 C 语言开发环境](https://www.onlinegdb.com/online_c_compiler)
* [在线流程图制作](https://yuml.me/diagram/scruffy/activity/draw)
* [在线评测系统，包括源码，支持 C ，C++ ，Python ， java ](https://docs.onlinejudge.me/)

---

* [青少年电子信息科普创新服务平台](http://kp.cie-info.org.cn/)
* [青少年机器人技术等级考试官网](http://qceit.org.cn/)
