注：0 = 无作案 ， 1 = 作案 。
## 代码如下：
```
#include <stdio.h>
#include <stdlib.h>

int main( int a , int b , int c , int d , int e , int f)
{
	printf("1 = 作案 \n0 = 无作案 \n");
	for ( a = 0 ; a <= 1 ; a ++ )
	for ( b = 0 ; b <= 1 ; b ++ )
	for ( c = 0 ; c <= 1 ; c ++ )
	for ( d = 0 ; d <= 1 ; d ++ )
	for ( e = 0 ; e <= 1 ; e ++ )
	for ( f = 0 ; f <= 1 ; f ++ )
	{
		if ( a + b == 0 ) continue;
		if ( a + d == 2 ) continue;
		if ( a + e + f != 2) continue;
		if ( b != c ) continue;
		if ( c + d != 1 ) continue ;
		if ( d == 0 && e == 1 ) continue;
		
		printf("a=%d\nb=%d\nc=%d\nd=%d\ne=%d\nf=%d\n",a,b,c,d,e,f);
	}
	return 0 ;
}
```


### 代码实现 2
```
#include <stdio.h>
#include <stdlib.h>

int main( int a , int b , int c , int d , int e , int f )
{
	printf("1 = 作案 \n0 = 无作案 \n");
	for ( a = 0 ; a <= 1 ; a ++ )
	for ( b = 0 ; b <= 1 ; b ++ )
	for ( c = 0 ; c <= 1 ; c ++ )
	for ( d = 0 ; d <= 1 ; d ++ )
	for ( e = 0 ; e <= 1 ; e ++ )
	for ( f = 0 ; f <= 1 ; f ++ )
	{
		if ( ( a  +  b  >  0 ) &&
		     ( a  +  d  <  2 ) && 
		     ( a + e + f == 2) &&
		     ( c + d     == 1) &&
		     ( b == c )        &&
		     ( d == 1 || ( d == 0 && e==0 ) ) )
		{
			printf("a=%d\nb=%d\nc=%d\nd=%d\ne=%d\nf=%d\n",a,b,c,d,e,f);
		}
	}
	return 0 ;
}
```
