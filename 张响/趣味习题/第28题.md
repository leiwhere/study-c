## 代码如下：
```
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int a , b , c , d ;
	for ( a = 1 ; a <= 4 ; a ++ )
	{
		for ( b = 1 ; b <= 4 ; b ++ )
		{
			for ( c = 1 ; c <= 4 ; c ++ )
			{
				for ( d = 1 ; d <= 4 ; d ++ )
				{
					if ( a + b + c + d != 10 || a * b * c * d != 24 )
					{
						continue ;
					}


					if (    ( ( a == 1) + (b == 3) == 1 ) &&
						( ( c == 1) + (d == 3) == 1 ) &&
						( ( d == 1) + (b == 3) == 1 ) 	 )
					{
						printf("a = %d , b = %d , c = %d , d = %d .\n", a , b , c , d );
					}
				}
			}
		}
	}
	return 0 ;
}
```
## 代码2:
```
#include <stdio.h>
int main(){
	int a , b , c , d ;
	for ( a = 1 ; a <= 4 ; a ++ ){
	for ( b = 1 ; b <= 4 ; b ++ ){
	for ( c = 1 ; c <= 4 ; c ++ ){
	for ( d = 1 ; d <= 4 ; d ++ ){
		if ( a*b*c*d==24 && a+b+c+d==10 ){
			if ( (a==1)+(b==3)&&(c==1)+(d==3)&&(d==1)+(b==3) == 1){
				printf("a=%d\nb=%d\nc=%d\nd=%d\n\n",a,b,c,d);
			}
		}
	}
	}
	}
	}
}
```
